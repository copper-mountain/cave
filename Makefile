
define build_image
	cd cuprum && \
		./scripts/mkimg --registry=$(GITLAB_REGISTRY_URL) --image=$(1) --tag=$(2)
endef

define push_image
	$(call build_image,$(1),$(2))
	cd cuprum && \
		./scripts/syncimg --registry=$(GITLAB_REGISTRY_URL) --login=$(GITLAB_USERNAME) --key=$(GITLAB_KEY) --insecure --image=$(1):$(2) --push
endef


clone-cuprum:
	if [ ! -d ./cuprum ]; then git clone https://gitlab.com/copper-mountain/cuprum.git; fi


gitlab-builder-test: clone-cuprum
	$(call build_image,gitlab-builder,1.0)

gitlab-builder: clone-cuprum
	$(call push_image,gitlab-builder,1.0)

devimg-test: clone-cuprum
	$(call build_image,devimg,1.0)

devimg: clone-cuprum
	$(call push_image,devimg,1.0)


tests: gitlab-builder-test devimg-test

all: gitlab-builder devimg

clean:
	rm -rf cuprum

.PHONY: all tests gitlab-builder-test gitlab-builder devimg-test devimg clean
